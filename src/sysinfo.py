
import sys
import platform

# https://pymotw.com/2/platform/
def platform_info():
#def sys_info_cpu():
    print("Platform information:")
    print('uname:', platform.uname())
    print()
    print('system   :', platform.system())
    print('node     :', platform.node())
    print('release  :', platform.release())
    print('version  :', platform.version())
    print('machine  :', platform.machine())
    print('processor:', platform.processor())

#print()
#print('Version      :', platform.python_version())
#print('Version tuple:', platform.python_version_tuple())
#print('Compiler     :', platform.python_compiler())
#print('Build        :', platform.python_build())




def main(argv=None):
    if argv is None:
        argv = sys.argv

    #sys_info_cpu()
    platform_info()
    
    return 0

